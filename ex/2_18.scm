(define (reverse list)
  (if (null? list)
      nil
      (cons (reverse (cdr list))
            (car list))
   ))
