(define (my_foreach proc items)
  (if (null? items)
      nil
      ((proc (car items))
       (my_foreach (cdr items)))))
