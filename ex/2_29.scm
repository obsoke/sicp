; Exercise 2.29: A binary mobile consists of two branches, a left branch and a
; right branch. Each branch is a rod of a certain length, from which hangs
; either a weight or another binary mobile. We can represent a binary mobile
; using compound data by constructing it from two branches (for example, using
; list):
(define (make-mobile left right)
  (list left right))

; A branch is constructed from a length (which must be a number) together with a
; structure, which may be either a number (representing a simple weight) or
; another mobile:
(define (make-branch length structure)
  (list length structure))

; Write the corresponding selectors left-branch and right-branch, which return
; the branches of a mobile, and branch-length and branch-structure, which return
; the components of a branch.
(define (left-branch mobile)
  (car mobile))
(define (right-branch mobile)
  (cadr mobile))

(define (branch-length branch)
  (car branch))
(define (branch-structure branch)
  (cadr branch))

; Utility Function - Create a mobile to use in testing!!
(define (create-mobile)
  (make-mobile
   (make-branch 5 5)
   (make-branch 5 5)))


; Using your selectors, define a procedure total-weight that returns the total
; weight of a mobile.
(define (total-weight mobile)
  (+
   (if (nil? (left-branch mobile))
       )))

(define (balanced? branch)
  (if (pairs? branch)
      (balanced? (branch-structure branch))
      (branch-structure branch)))
