;; Observe that our model of evaluation allows for combinations whose operators
;; are compound expressions. Use this observation to describe the behavior of the
;; following procedure:

(define (a-plus-abs-b a b)
  ((if (> b 0) + -) a b))

;; To more easily understand what's going on, let's add some lines and tabs to
;; our expression:

(define (a-plus-abs-b a b)
  (
   (if (> b 0)
       +
       -)
   a
   b))

;; Our procedure ~a-plus-abs-b~ takes two arguments: a and b. The if expression
;; will return the correct operator to use in order to add ~a~ to the absolute
;; value of ~b~. If ~b~ is a number above 0, addition between ~a~ and ~b~ is
;; performed. If ~b~ is less than 0 (or 0, but that doesn't really count),
;; substraction is used.
