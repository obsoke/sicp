;; Define a procedure that takes three numbers as arguments and returns the sum
;; of the squares of the two larger numbers.

(define (square x) (* x x))
(define (square-summer a b c)
  (+ (square (if (> a b)
                 a
                 b))
     (square (if (> a c)
                 a
                 c)))
  )
(square-summer 1 2 3) ; 13
