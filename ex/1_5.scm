;Ben Bitdiddle has invented a test to determine whether the interpreter he is
;faced with is using applicative-order evaluation or normal-order evaluation. He
;defines the following two procedures:

(define (p) (p))

(define (test x y)
  (if (= x 0)
      0
      y))

;; Then he evaluates the expression

(test 0 (p))

;What behavior will Ben observe with an interpreter that uses applicative-order
;evaluation? What behavior will he observe with an interpreter that uses
;normal-order evaluation? Explain your answer. (Assume that the evaluation rule
;for the special form if is the same whether the interpreter is using normal or
;applicative order: The predicate expression is evaluated first, and the result
;determines whether to evaluate the consequent or the alternative expression.)

; If the application used applicative-order evaluation, first both the operator
; and operands would be evaluated. Then the ~test~ procedure would be called
; with the arguments ~0~ and ~(p)~. Inside the predicate of the ~if~ expression,
; all the operators & operands would once again be evaluated, and then the first
; branch of the ~if~ would return ~0~.

; If the application used normal-order evaluation, the expression ~(test 0
; (p))~ would be expanded into the following:

(if (= 0 0)
    0
    (p))

; Before being reduced into the same answer of ~0~.

