# SICP

These are my notes & exercises from *Structure and Interpretation of Computer
Programs* by Gerald Jay Sussman and Hal Abelson. The exercises start off being
written in MIT Scheme 9.2, but then are moved to Racket v6.10 using the `#lang
sicp` directive.

Any comments or suggestions are welcome via an issue or pull request!
